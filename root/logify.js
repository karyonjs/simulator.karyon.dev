import { State } from '/karyon.js';

const isFn = any => typeof any === 'function';
const isPrimitive = any => typeof (any ?? 1) !== 'object' && !isFn(any);

const origin = new RegExp(window.location.origin, 'g');

const errorify = (stackify => value => {
    const error = value instanceof ErrorEvent ? value.error ?? value : value;
    let {name = 'Exception', message, stack} = error;
    message ??= error;
    if (value instanceof ErrorEvent) {
        const {filename: f, lineno: l, colno: c} = value;
        stack = `${stackify(f)}:${l}:${c}`;
    } else
        stack = stack?.split('\n').slice(1).map(stackify).join('\n') ?? '';
    return {[name]: `${message}`.replace(origin, ''), stack};
})(stack => stack.replace(origin, '').replace(/\s*at /g, ''));

const digest = (out => function (keys, refs, replaces, [key, value]) {
    if (State.owns(value))
        return State.peek(value);
    if (value instanceof Error || value instanceof ErrorEvent)
        return errorify(value);
    if (value instanceof Node)
        return out(replaces, match => {
            const text = (value instanceof Element ? value.outerHTML :
                value instanceof ShadowRoot ? value.innerHTML :
                value.textContent).replace(/\n+/g, '');
            return {match, text};
        });
    if (typeof value === 'function')
        return out(replaces, match => {
            match = key ? `"${key}": ${match}` : match;
            let text = value.toString();
            if (key === 'function' || key && !text.startsWith(key))
                text = `"${key}": ${text}`;
            return {match, text};
        });
    if (typeof value === 'string' || typeof value === 'symbol')
        return out(replaces, match => ({match, text: `"${value.toString()}"`}));
    if (!value || typeof value !== 'object')
        return value;
    keys.set(key, (keys.get(key) ?? 0) + 1);
    const ref = refs.get(value);
    if (!ref) {
        refs.set(value, {key, id: keys.get(key)});
        return value;
    }
    return out(replaces, match => {
        const text = `*ref:${ref.key}:${ref.id}`;
        return {match, text};
    });
})((ID => (replaces, fn) => {
    const id = `(${ID}:${replaces.length})`;
    replaces.push(fn(`"${id}"`));
    return id;
})(`${Math.random()}`.slice(2, 8)));

const logify = value => {
    if (isPrimitive(value))
        return (value ?? `${value}`).toString();
    const keys = new Map(), refs = new Map(), replaces = [];
    let text = JSON.stringify(value, function () {
        return digest.call(this, keys, refs, replaces, arguments);
    }, 2);
    for (const r of replaces)
        text = text.replace(r.match, r.text);
    return text;
};

export default logify;
