const logger = Object.create(null);

'dir log info warn error'.split(' ').forEach(name => {
    const fn = console[name], raise = console.error;
    console[name] = function (...args) {
        try { logger.log?.(name, ...args); } catch (e) { raise(e); }
        return fn.apply(this, args);
    };
});

export default logger;
