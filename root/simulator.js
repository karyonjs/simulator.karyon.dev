import logger from '/logger.js';
import logify from '/logify.js';

const reload = (src => data =>
    (data?.src ?? src) === src && location.reload())(location.href);

const ipc = (queue => ({
    emit (event, ...args) {
        this.io ? this.io.postMessage({
            event, log: args.map(arg => logify(arg)).join(' ')
        }) : queue.unshift([event, ...args]).length > 1000 && queue.pop();
    },
    flush () { queue.splice(0).reverse().forEach(args => this.emit(...args)); }
}))([]);

const swc = ((entity, io) => {
    io.addEventListener('message', (events => message => {
        const {entity: id, event, args = []} = message.data ?? {};
        if (!id || id === entity)
            return;
        const task = events[event];
        task ? task(args) : ipc.emit(event, ...args);
    })({
        'sw.pong' () { ipc.emit('ready'); },
        'cache.ready' (args) { reload(args[0]); }
    }));
    return {submit: async (event, data) => {
        try { await fetch('/'), io.postMessage({entity, event, data}); }
        catch {}
    }};
})('simulator', new BroadcastChannel('service-worker'));

logger.log = (...args) => ipc.emit(...args);

addEventListener('message', (events => message => {
    const {event, data} = message.data ?? {};
    ipc.io = message.ports[0], events[event]?.(data);
})({
    init () { swc.submit('sw.ping'); },
    ready () { ipc.flush(); },
    start (data) { swc.submit('cache.update', data); },
    reset (data) { swc.submit('cache.delete', data); },
    reload (data) { reload(data); }
}));
addEventListener('error', event => ipc.emit('error', event));

try { (await navigator.serviceWorker.register('/sw.js')).update(); }
catch (e) { ipc.emit('error', e); }
