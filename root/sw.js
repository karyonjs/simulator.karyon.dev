const VERSION = '0';

const cacheSelect = (redirect => event => {
    const req = event.request;
    const url = new URL(req.url);
    const uri = url.pathname;
    const path = uri.endsWith('/') ? '/' : url;
    if (req.method !== 'GET' || !/^http/.test(url.protocol) ||
        !/karyon\.dev$/.test(url.origin))
        return;
    event.respondWith(caches.match(path).then(res => {
        if (res)
            return res.clone();
        const main = uri.match(/(\/main\.(js|css))$/);
        if (main && main[0] !== uri)
            return redirect(main[0]);
        return fetch(path).then(res => {
            if (res.status !== 200)
                return redirect();
            if (uri.lastIndexOf('/') === 0)
                caches.open('*').then(cache => cache.put(path, res.clone()));
            return res.clone();
        }).catch(() => redirect());
    }).catch(() => redirect()));
})(path => Response.redirect(path ?? '/', 302));

const cacheUpdate = data => {
    const {src, files} = data;
    caches.delete(src).then(() => caches.open(src).then(cache => {
        Promise.all(files.map(file =>
            cache.put(file.path, new Response(file.content, {
                status: 200, statusText: 'OK',
                headers: {'Content-Type': file.type}
            }))
        )).then(() => swc.submit('cache.ready', {src}));
    }));
};

const cacheDelete = data => {
    const {src, match} = data ?? {};
    const regex = typeof match === 'string' ? new RegExp(match, 'gi') : null;
    return caches.keys().then(keys => Promise.all(keys.map(key =>
        (!src || src === key) && caches.open(key)
        .then(cache => cache.keys().then(reqs =>
            Promise.all(reqs.map(req => {
                const path = new URL(req.url).pathname;
                return !regex?.test(path) && cache.delete(req);
        })).then(() => cache.keys()
            .then(reqs => !reqs.length && caches.delete(key))
        )))
    )).then(() => swc.submit('cache.ready', {src})));
};

const swc = (() => {

    const entity = 'service-worker';
    const port = new BroadcastChannel(entity);
    
    const submit = (event, ...args) => {
        port.postMessage({entity, event, args});
    };
    
    port.addEventListener('message', (events => message => {
        const {entity: id, event, data} = message.data ?? {};
        id && id !== entity && events[event]?.(data);
    })({
        'sw.ping': () => submit('sw.pong'),
        'cache.update': cacheUpdate,
        'cache.delete': cacheDelete
    }));

    return {submit};
})();

self.addEventListener('install', () => self.skipWaiting());
self.addEventListener('activate', event => event.waitUntil(cacheDelete()));
self.addEventListener('fetch', cacheSelect);
