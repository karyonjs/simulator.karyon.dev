import env, { insert } from '/karyon.js';

insert({id: 'karyon', content: [
    {id: 'logo'}, {id: 'version', content: `v${env.VERSION}`}
]}, document.body);
